package com.maestroes.home_directory.helper;

import android.content.Context;

import io.paperdb.Paper;

public class Constants {


    public static final String userID = "userID";
    public static final String CONTACTS = "contacts" ;
    public static String isSocialSignUp = "socialSignUP";


    public static String getUserID(Context context) {

        Paper.init(context);
        return Paper.book().read(Constants.userID);


    }


//    public static getPaperDatabase(Context context,String key) {
//
//        Paper.init(context);
//        return Paper.book().read(key);
//
//
//
//    }


}
