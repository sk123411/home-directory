package com.maestroes.home_directory.helper;

public class Resource<T> {

    private T data;
    private String message;

    public Resource(T data, String message) {

        this.data = data;
        this.message = message;
    }


    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static class Success<T>  extends Resource<T>{
             private String data;

        public Success(T data, String message) {
            super(data, null);
        }
    }


    public static class Error<T>  extends Resource<T>{
        private String message;

        public Error(T data, String message) {
            super(null, message);
        }
    }



}
