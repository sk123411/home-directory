package com.maestroes.home_directory.Model;

public class ContactModel {
    private String name;
    private int image;
    private String number;

    public ContactModel(String name, int image, String number) {
        this.name = name;
        this.image = image;
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }
}
