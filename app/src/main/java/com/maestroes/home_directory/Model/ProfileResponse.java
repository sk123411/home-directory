package com.maestroes.home_directory.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;



public class ProfileResponse {

    @SerializedName("result")
    @Expose
    private Boolean result;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private Data data;

    public Boolean getResult() {
        return result;
    }

    public void setResult(Boolean result) {
        this.result = result;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }



    public class Data {

        @SerializedName("userID")
        @Expose
        private String userID;
        @SerializedName("email")
        @Expose
        private String email;
        @SerializedName("mobile")
        @Expose
        private String mobile;
        @SerializedName("password")
        @Expose
        private String password;
        @SerializedName("fname")
        @Expose
        private String fname;
        @SerializedName("lname")
        @Expose
        private String lname;
        @SerializedName("mname")
        @Expose
        private String mname;
        @SerializedName("profile_image")
        @Expose
        private String profileImage;
        @SerializedName("gender")
        @Expose
        private String gender;
        @SerializedName("whatsapp_no")
        @Expose
        private String whatsappNo;
        @SerializedName("address")
        @Expose
        private String address;
        @SerializedName("dob")
        @Expose
        private String dob;
        @SerializedName("website")
        @Expose
        private String website;
        @SerializedName("about")
        @Expose
        private String about;
        @SerializedName("strtotime")
        @Expose
        private String strtotime;
        @SerializedName("occupationID")
        @Expose
        private String occupationID;
        @SerializedName("company_name")
        @Expose
        private String companyName;
        @SerializedName("department")
        @Expose
        private String department;
        @SerializedName("title")
        @Expose
        private String title;
        @SerializedName("office_address")
        @Expose
        private String officeAddress;
        @SerializedName("office_no")
        @Expose
        private String officeNo;
        @SerializedName("dom")
        @Expose
        private String dom;
        @SerializedName("spouse_name")
        @Expose
        private String spouseName;
        @SerializedName("spouse_pic")
        @Expose
        private String spousePic;
        @SerializedName("spouse_DOB")
        @Expose
        private String spouseDOB;
        @SerializedName("spouse_mobile_number")
        @Expose
        private String spouseMobileNumber;
        @SerializedName("spouse_whatsapp_number")
        @Expose
        private String spouseWhatsappNumber;
        @SerializedName("path")
        @Expose
        private String path;

        public String getUserID() {
            return userID;
        }

        public void setUserID(String userID) {
            this.userID = userID;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getFname() {
            return fname;
        }

        public void setFname(String fname) {
            this.fname = fname;
        }

        public String getLname() {
            return lname;
        }

        public void setLname(String lname) {
            this.lname = lname;
        }

        public String getMname() {
            return mname;
        }

        public void setMname(String mname) {
            this.mname = mname;
        }

        public String getProfileImage() {
            return profileImage;
        }

        public void setProfileImage(String profileImage) {
            this.profileImage = profileImage;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getWhatsappNo() {
            return whatsappNo;
        }

        public void setWhatsappNo(String whatsappNo) {
            this.whatsappNo = whatsappNo;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getDob() {
            return dob;
        }

        public void setDob(String dob) {
            this.dob = dob;
        }

        public String getWebsite() {
            return website;
        }

        public void setWebsite(String website) {
            this.website = website;
        }

        public String getAbout() {
            return about;
        }

        public void setAbout(String about) {
            this.about = about;
        }

        public String getStrtotime() {
            return strtotime;
        }

        public void setStrtotime(String strtotime) {
            this.strtotime = strtotime;
        }

        public String getOccupationID() {
            return occupationID;
        }

        public void setOccupationID(String occupationID) {
            this.occupationID = occupationID;
        }

        public String getCompanyName() {
            return companyName;
        }

        public void setCompanyName(String companyName) {
            this.companyName = companyName;
        }

        public String getDepartment() {
            return department;
        }

        public void setDepartment(String department) {
            this.department = department;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getOfficeAddress() {
            return officeAddress;
        }

        public void setOfficeAddress(String officeAddress) {
            this.officeAddress = officeAddress;
        }

        public String getOfficeNo() {
            return officeNo;
        }

        public void setOfficeNo(String officeNo) {
            this.officeNo = officeNo;
        }

        public String getDom() {
            return dom;
        }

        public void setDom(String dom) {
            this.dom = dom;
        }

        public String getSpouseName() {
            return spouseName;
        }

        public void setSpouseName(String spouseName) {
            this.spouseName = spouseName;
        }

        public String getSpousePic() {
            return spousePic;
        }

        public void setSpousePic(String spousePic) {
            this.spousePic = spousePic;
        }

        public String getSpouseDOB() {
            return spouseDOB;
        }

        public void setSpouseDOB(String spouseDOB) {
            this.spouseDOB = spouseDOB;
        }

        public String getSpouseMobileNumber() {
            return spouseMobileNumber;
        }

        public void setSpouseMobileNumber(String spouseMobileNumber) {
            this.spouseMobileNumber = spouseMobileNumber;
        }

        public String getSpouseWhatsappNumber() {
            return spouseWhatsappNumber;
        }

        public void setSpouseWhatsappNumber(String spouseWhatsappNumber) {
            this.spouseWhatsappNumber = spouseWhatsappNumber;
        }

        public String getPath() {
            return path;
        }

        public void setPath(String path) {
            this.path = path;
        }


        @Override
        public String toString() {
            return "Data{" +
                    "userID='" + userID + '\'' +
                    ", email='" + email + '\'' +
                    ", mobile='" + mobile + '\'' +
                    ", password='" + password + '\'' +
                    ", fname='" + fname + '\'' +
                    ", lname='" + lname + '\'' +
                    ", mname='" + mname + '\'' +
                    ", profileImage='" + profileImage + '\'' +
                    ", gender='" + gender + '\'' +
                    ", whatsappNo='" + whatsappNo + '\'' +
                    ", address='" + address + '\'' +
                    ", dob='" + dob + '\'' +
                    ", website='" + website + '\'' +
                    ", about='" + about + '\'' +
                    ", strtotime='" + strtotime + '\'' +
                    ", occupationID='" + occupationID + '\'' +
                    ", companyName='" + companyName + '\'' +
                    ", department='" + department + '\'' +
                    ", title='" + title + '\'' +
                    ", officeAddress='" + officeAddress + '\'' +
                    ", officeNo='" + officeNo + '\'' +
                    ", dom='" + dom + '\'' +
                    ", spouseName='" + spouseName + '\'' +
                    ", spousePic='" + spousePic + '\'' +
                    ", spouseDOB='" + spouseDOB + '\'' +
                    ", spouseMobileNumber='" + spouseMobileNumber + '\'' +
                    ", spouseWhatsappNumber='" + spouseWhatsappNumber + '\'' +
                    ", path='" + path + '\'' +
                    '}';
        }
    }

    @Override
    public String toString() {
        return "ProfileResponse{" +
                "result=" + result +
                ", message='" + message + '\'' +
                ", data=" + data +
                '}';
    }
}
