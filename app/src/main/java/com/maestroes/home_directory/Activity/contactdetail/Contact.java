package com.maestroes.home_directory.Activity.contactdetail;

import com.maestroes.home_directory.Model.DeleteUserResponse;
import com.maestroes.home_directory.Model.ListProfileResponse;
import com.maestroes.home_directory.Model.ProfileResponse;
import com.maestroes.home_directory.helper.ApiResponse;

import java.io.File;
import java.util.HashMap;

public interface Contact {

   void getProfile(String control, HashMap<String,String> data, ApiResponse<ProfileResponse> apiResponse);
   void changeProfile(String control, HashMap<String,String> data, File file,ApiResponse<ProfileResponse> apiResponse);



   void updateName(String control, HashMap<String,String> data, ApiResponse<ProfileResponse> apiResponse);

   void updateEmail(String control, HashMap<String,String> data, ApiResponse<ProfileResponse> apiResponse);
   void updateDOB(String control, HashMap<String,String> data, ApiResponse<ProfileResponse> apiResponse);
   void updatePassword(String control, HashMap<String,String> data, ApiResponse<ProfileResponse> apiResponse);
   void updateNumber(String control, HashMap<String,String> data, ApiResponse<ProfileResponse> apiResponse);

   void getListProfiles(String control, HashMap<String,String> data, ApiResponse<ListProfileResponse> listProfileResponseApiResponse);

   void deleteUser(String control, HashMap<String,String> data, ApiResponse<DeleteUserResponse> apiResponse);

   void addBlockUser(String control, HashMap<String,String> data, ApiResponse<DeleteUserResponse> apiResponse);


}
