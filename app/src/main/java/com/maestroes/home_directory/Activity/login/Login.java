package com.maestroes.home_directory.Activity.login;

import android.content.Intent;

import androidx.appcompat.app.AppCompatActivity;

import com.maestroes.home_directory.Model.ProfileResponse;
import com.maestroes.home_directory.Model.SignUpResponse;
import com.maestroes.home_directory.helper.ApiResponse;

import org.json.JSONObject;

import java.util.HashMap;

public interface Login {

   void login(String control, HashMap<String,String> data, ApiResponse<SignUpResponse> apiResponse);


   Intent googleSignIn(AppCompatActivity appCompatActivity);

   void SignInSocial(String control, HashMap<String,String> data, ApiResponse<ProfileResponse> apiResponse);

   void forgetPassword(String control, HashMap<String,String> data, ApiResponse<JSONObject> apiResponse);


}
