package com.maestroes.home_directory.Activity.signup;

import android.content.Context;

import com.androidnetworking.AndroidNetworking;
import com.maestroes.home_directory.Model.SignUpResponse;
import com.maestroes.home_directory.helper.ApiResponse;
import com.rx2androidnetworking.Rx2AndroidNetworking;

import java.util.HashMap;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class SignUpImplementation implements SignUP {
        private Context application;

      public  SignUpImplementation(Context a){
          this.application = a;
      }


    @Override
    public void SignUp(String control, HashMap<String,String> data, ApiResponse<SignUpResponse> apiResponse) {
        final SignUpResponse[] signUpResponseTemp = new SignUpResponse[1];

        AndroidNetworking.initialize(application);


        Rx2AndroidNetworking.post("https://maestros.co.in/Home_diractory/appservice/process.php")
                .addBodyParameter("control","signup")
                .addBodyParameter("name",data.get("name"))
                .addBodyParameter("fname",data.get("name"))
                .addBodyParameter("mobile",data.get("mobile"))
                .addBodyParameter("email",data.get("email"))
                .addBodyParameter("password",data.get("password"))

                .build().

                getObjectObservable(SignUpResponse.class).observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io()).subscribe(new Observer<SignUpResponse>() {
            @Override
            public void onSubscribe(@NonNull Disposable d) {

            }

            @Override
            public void onNext(@NonNull SignUpResponse signUpResponse) {
                apiResponse.data(signUpResponse);
            }

            @Override
            public void onError(@NonNull Throwable e) {
                apiResponse.Error(e.getMessage());

            }

            @Override
            public void onComplete() {

            }
        });


    }
}
