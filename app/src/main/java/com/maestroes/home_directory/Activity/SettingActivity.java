package com.maestroes.home_directory.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.maestroes.home_directory.Activity.contactdetail.ContactViewModel;
import com.maestroes.home_directory.Model.ProfileResponse;
import com.maestroes.home_directory.R;
import com.maestroes.home_directory.databinding.ActivitySettingBinding;
import com.maestroes.home_directory.helper.Constants;

public class SettingActivity extends AppCompatActivity {

    RelativeLayout relative1;
    ImageView user1;

    ActivitySettingBinding binding;
    private ContactViewModel contactViewModel;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivitySettingBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        contactViewModel = ViewModelProviders.of(this).get(ContactViewModel.class);


        contactViewModel.getProfile(Constants.getUserID(getApplicationContext()), getApplicationContext());



        binding.editPassword.setEnabled(false);
        binding.editMobile.setEnabled(false);



        contactViewModel.getResponse().observe(this, new Observer<ProfileResponse>() {
            @Override
            public void onChanged(ProfileResponse signUpResponse) {


                if (signUpResponse != null) {
                    Toast.makeText(getApplicationContext(), "Profile updated", Toast.LENGTH_LONG).show();
                    binding.editPassword.setEnabled(false);
                    binding.editMobile.setEnabled(false);

                    binding.editPassword.setText(signUpResponse.getData().getPassword());
                    binding.editMobile.setText(signUpResponse.getData().getMobile());


                }


            }
        });







        contactViewModel.getLoginEvent().observe(this, new Observer<ContactViewModel.ProfileEvent>() {
            @Override
            public void onChanged(ContactViewModel.ProfileEvent profileEvent) {

                switch (profileEvent) {

                    case DATA:
                        binding.progressBar.setVisibility(View.GONE);
                        break;
                    case ERROR:
                        Toast.makeText(getApplicationContext(), "Servor error occured, Please try later", Toast.LENGTH_LONG).show();
                        binding.progressBar.setVisibility(View.GONE);
                        break;
                    case LOADING:
                        binding.progressBar.setVisibility(View.VISIBLE);
                        break;

                }

            }
        });







        binding.editPasswordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!binding.editPassword.isEnabled()) {
                    binding.editPassword.setEnabled(true);
                    binding.editPassword.requestFocus();
                    binding.editPassword.setText("");
                    binding.editPassword.setHint("Enter new password");
                    return;
                } else {

                    if (!binding.editPassword.getText().toString().isEmpty()) {


                        contactViewModel.updatePassword(Constants.getUserID(getApplicationContext()), binding.editPassword.getText().toString(),
                                getApplicationContext());
                    } else {
                        Toast.makeText(getApplicationContext(), "Please enter password to update", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });




        binding.editMobileButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!binding.editMobile.isEnabled()) {
                    binding.editMobile.setEnabled(true);
                    binding.editMobile.requestFocus();
                    binding.editMobile.setText("");
                    binding.editMobile.setHint("Enter new mobile");
                    return;
                } else {

                    if (!binding.editMobile.getText().toString().isEmpty()) {


                        contactViewModel.updateNumber(Constants.getUserID(getApplicationContext()), binding.editMobile.getText().toString(),
                                getApplicationContext());
                    } else {
                        Toast.makeText(getApplicationContext(), "Please enter mobile to update", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });



    }
}