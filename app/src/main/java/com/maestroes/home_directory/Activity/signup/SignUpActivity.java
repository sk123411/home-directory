package com.maestroes.home_directory.Activity.signup;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.maestroes.home_directory.Activity.login.LoginActivity;
import com.maestroes.home_directory.Model.SignUpResponse;
import com.maestroes.home_directory.databinding.ActivitySignUpBinding;
import com.google.android.material.button.MaterialButton;

public class SignUpActivity extends AppCompatActivity {

    MaterialButton button_signup;
    private SignUpViewModel signUpViewModel;
    private ActivitySignUpBinding signUpBinding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        signUpBinding = ActivitySignUpBinding.inflate(getLayoutInflater());
        setContentView(signUpBinding.getRoot());

        signUpViewModel = ViewModelProviders.of(this).get(SignUpViewModel.class);



        signUpViewModel.getResponse().observe(this, new Observer<SignUpResponse>() {
            @Override
            public void onChanged(SignUpResponse signUpResponse) {


                if (signUpResponse!=null) {
                    Toast.makeText(getApplicationContext(), "Sign up successfull! You can login with credentials", Toast.LENGTH_LONG).show();

                    startActivity(new Intent(getApplicationContext(), LoginActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));


                }


            }
        });



        signUpBinding.loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                startActivity(new Intent(getApplicationContext(),LoginActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));

            }
        });


        signUpViewModel.getSignUpEvent().observe(this, new Observer<SignUpViewModel.SignUpEvent>() {
            @Override
            public void onChanged(SignUpViewModel.SignUpEvent signUpEvent) {

                switch (signUpEvent){

                    case DATA:
                        signUpBinding.progressBar.setVisibility(View.GONE);
                        break;
                    case ERROR:
                        Toast.makeText(getApplicationContext(),"Servor error occured, Please try later",Toast.LENGTH_LONG).show();
                        signUpBinding.progressBar.setVisibility(View.GONE);
                        break;
                    case LOADING:
                        signUpBinding.progressBar.setVisibility(View.VISIBLE);
                        break;

                }

            }
        });




        signUpBinding.signupButton.setOnClickListener(

                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        if(!signUpBinding.editEmail.getText().toString().contains("@")){

                            signUpBinding.editEmail.setError("Please enter valid email");
                        }else {


                            if (signUpBinding.editName.getText().toString() != null &&
                                    signUpBinding.editNumber.getText().toString() != null &&
                                    signUpBinding.editEmail.getText().toString() != null &&
                                    signUpBinding.editPassword.getText().toString() != null) {

                                signUpViewModel.signUp(
                                        signUpBinding.editName.getText().toString(),
                                        signUpBinding.editEmail.getText().toString(),
                                        signUpBinding.editNumber.getText().toString(),
                                        signUpBinding.editPassword.getText().toString(),
                                        getApplicationContext()
                                );


                            }else {


                                Toast.makeText(getApplicationContext(),"Please fill all the fields",Toast.LENGTH_LONG).show();

                            }
                        }
                    }
                }

        );



    }
}