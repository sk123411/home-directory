package com.maestroes.home_directory.Activity.signup;

import android.content.Context;
import android.widget.Toast;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.maestroes.home_directory.Model.SignUpResponse;
import com.maestroes.home_directory.helper.ApiResponse;

import java.util.HashMap;

public class SignUpViewModel extends ViewModel {


    private SignUpImplementation signUpImplementation;

    private MutableLiveData<SignUpResponse> signUpResponseMutableLiveData=  new MutableLiveData<>();

    private MutableLiveData<SignUpEvent> signUpEventMutableLiveData=  new MutableLiveData<>();





     public enum  SignUpEvent{

         DATA,ERROR,LOADING
    }





    public void signUp(String name, String email, String mobile, String password, Context context){

        signUpEventMutableLiveData.setValue(SignUpEvent.LOADING);

        HashMap<String,String> map = new HashMap<>();

        signUpImplementation = new SignUpImplementation(context);

        map.put("name",name);
        map.put("email",email);
        map.put("mobile",mobile);
        map.put("password",password);


      signUpImplementation.SignUp("signup", map, new ApiResponse<SignUpResponse>() {
            @Override
            public void data(SignUpResponse data) {

                if (data.getResult()) {
                    signUpEventMutableLiveData.setValue(SignUpEvent.DATA);

                    signUpResponseMutableLiveData.setValue(data);
                }else {
                    Toast.makeText(context,""+data.getMessage(),Toast.LENGTH_LONG).show();

                    signUpEventMutableLiveData.setValue(SignUpEvent.ERROR);

                }

                }

          @Override
          public void Error(String message) {
              signUpEventMutableLiveData.setValue(SignUpEvent.ERROR);

          }
      });





    }





    public LiveData<SignUpResponse> getResponse() {
        return signUpResponseMutableLiveData;
    }



    public LiveData<SignUpEvent> getSignUpEvent() {
        return signUpEventMutableLiveData;
    }


}
