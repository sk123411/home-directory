package com.maestroes.home_directory.Activity.contactdetail;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.Manifest;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.github.dhaval2404.imagepicker.ImagePicker;
import com.github.dhaval2404.imagepicker.util.FileUriUtils;
import com.google.firebase.auth.FirebaseAuth;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;
import com.maestroes.home_directory.Activity.ContactActivity;
import com.maestroes.home_directory.Activity.SettingActivity;
import com.maestroes.home_directory.Activity.login.LoginActivity;
import com.maestroes.home_directory.Model.DeleteUserResponse;
import com.maestroes.home_directory.Model.ProfileResponse;
import com.maestroes.home_directory.R;
import com.maestroes.home_directory.databinding.ActivityContactDetailsBinding;
import com.maestroes.home_directory.databinding.ActivityLoginBinding;
import com.maestroes.home_directory.helper.Constants;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.Calendar;

import io.paperdb.Paper;

public class ContactDetailsActivity extends AppCompatActivity {


    private ActivityContactDetailsBinding binding;
    private ContactViewModel contactViewModel;
    private boolean isCurrentUser = false;
    private String userId;
    private String userMobileNumber;
    private boolean isContactBlock = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_details);

        binding = ActivityContactDetailsBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        contactViewModel = ViewModelProviders.of(this).get(ContactViewModel.class);


        userId = getIntent().getStringExtra("id");
        isContactBlock = getIntent().getBooleanExtra("IsContactBlock",false);



        Log.d("PROFILEEEEEEEe", "::"+Constants.getUserID(getApplicationContext()));


        if (userId!=null){
            contactViewModel.getProfile(userId, getApplicationContext());


        }






        binding.toolbar.setTitle("");
        setSupportActionBar(binding.toolbar);



        binding.editName.setEnabled(false);
        binding.editEmail.setEnabled(false);
        binding.editMobile.setEnabled(false);
        binding.editDob.setEnabled(false);


        contactViewModel.getResponse().observe(this, new Observer<ProfileResponse>() {
            @Override
            public void onChanged(ProfileResponse signUpResponse) {


                if (signUpResponse != null) {


                    if (signUpResponse.getData().getUserID().equals(Constants.getUserID(getApplicationContext()))) {

                        isCurrentUser = true;


                        Log.d("SIGGGGGGGGGGGGG", ":::"+signUpResponse.getData());


                        invalidateOptionsMenu();


                        Toast.makeText(getApplicationContext(), "Profile updated", Toast.LENGTH_LONG).show();
                        binding.editName.setEnabled(false);
                        binding.editEmail.setEnabled(false);
                        binding.editMobile.setEnabled(false);
                        binding.editDob.setEnabled(false);


                        if (signUpResponse.getData().getFname() != null) {
                            binding.editName.setText(signUpResponse.getData().getFname());
                        }
                        binding.editEmail.setText(signUpResponse.getData().getEmail());
                        binding.editMobile.setText(signUpResponse.getData().getMobile());

                        if (!binding.editDob.getText().equals("")) {
                            binding.editDob.setText(signUpResponse.getData().getDob());
                        } else {
                            binding.editDob.setText("20/01/1989");

                        }


                        Picasso.with(getApplicationContext()).load(signUpResponse.getData().getPath()+signUpResponse.getData().getProfileImage()).placeholder(R.drawable.user3)
                                .into(binding.profileImage);


                        if (isCurrentUser) {

                            binding.profileImage.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {


                                    Dexter.withContext(ContactDetailsActivity.this)
                                            .withPermission(android.Manifest.permission.READ_EXTERNAL_STORAGE)
                                            .withListener(new PermissionListener() {
                                                @Override
                                                public void onPermissionGranted(PermissionGrantedResponse permissionGrantedResponse) {
                                                    ImagePicker.Companion.with(ContactDetailsActivity.this)
                                                            .crop()                    //Crop image(Optional), Check Customization for more option
                                                            .compress(1024)            //Final image size will be less than 1 MB(Optional)
                                                            .maxResultSize(1080, 1080)    //Final image resolution will be less than 1080 x 1080(Optional)
                                                            .start();

                                                }

                                                @Override
                                                public void onPermissionDenied(PermissionDeniedResponse permissionDeniedResponse) {

                                                }

                                                @Override
                                                public void onPermissionRationaleShouldBeShown(PermissionRequest permissionRequest, PermissionToken permissionToken) {

                                                }
                                            }).check();


                                }
                            });
                        }

                    }else {

                        isCurrentUser = false;
                        binding.editNameButton.setVisibility(View.GONE);
                        binding.editDobButton.setVisibility(View.GONE);
                        binding.editEmailButton.setVisibility(View.GONE);
                        binding.editMobileButton.setVisibility(View.GONE);


                        if (signUpResponse.getData().getFname() != null) {
                            binding.editName.setText(signUpResponse.getData().getFname());
                        }
                        binding.editEmail.setText(signUpResponse.getData().getEmail());
                        binding.editMobile.setText(signUpResponse.getData().getMobile());

                        if (!binding.editDob.getText().equals("")) {
                            binding.editDob.setText(signUpResponse.getData().getDob());
                        } else {
                            binding.editDob.setText("20/01/1989");

                        }

                        userMobileNumber = signUpResponse.getData().getMobile();


                    }

                    Picasso.with(getApplicationContext()).load(signUpResponse.getData().getPath()+signUpResponse.getData().getProfileImage()).placeholder(R.drawable.user3)
                            .into(binding.profileImage);



                }


            }
        });







        contactViewModel.getDeletedUserLiveData().observe(this, new Observer<DeleteUserResponse>() {
            @Override
            public void onChanged(DeleteUserResponse deleteUserResponse) {


                if (deleteUserResponse!=null){


                    if (deleteUserResponse.getResult()){


                        Toast.makeText(getApplicationContext(),"User status updated",Toast.LENGTH_LONG).show();

                        startActivity(new Intent(ContactDetailsActivity.this, ContactActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                    }else {
                        Toast.makeText(getApplicationContext(),"Error occured",Toast.LENGTH_LONG).show();


                    }
                }



            }
        });

        contactViewModel.getLoginEvent().observe(this, new Observer<ContactViewModel.ProfileEvent>() {
            @Override
            public void onChanged(ContactViewModel.ProfileEvent profileEvent) {

                switch (profileEvent) {

                    case DATA:
                        binding.progressBar.setVisibility(View.GONE);
                        break;
                    case ERROR:
                        Toast.makeText(getApplicationContext(), "Servor error occured, Please try later", Toast.LENGTH_LONG).show();
                        binding.progressBar.setVisibility(View.GONE);
                        break;
                    case LOADING:
                        binding.progressBar.setVisibility(View.VISIBLE);
                        break;

                }

            }
        });


        binding.editNameButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (!binding.editName.isEnabled()) {
                    binding.editName.setEnabled(true);
                    binding.editName.requestFocus();
                    binding.editName.setText("");
                    binding.editName.setHint("Enter new name");
                    return;
                } else {

                    if (!binding.editName.getText().toString().isEmpty()) {


                        contactViewModel.updateName(Constants.getUserID(getApplicationContext()), binding.editName.getText().toString(),
                                getApplicationContext());
                    } else {
                        Toast.makeText(getApplicationContext(), "Please enter name to update", Toast.LENGTH_LONG).show();
                    }
                }


            }


        });


        binding.editEmailButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (!binding.editEmail.isEnabled()) {
                    binding.editEmail.setEnabled(true);
                    binding.editEmail.requestFocus();
                    binding.editEmail.setText("");
                    binding.editEmail.setHint("Enter new email");
                    return;
                } else {

                    if (!binding.editEmail.getText().toString().isEmpty()) {


                        contactViewModel.updateEmail(Constants.getUserID(getApplicationContext()), binding.editEmail.getText().toString(),
                                getApplicationContext());
                    } else {
                        Toast.makeText(getApplicationContext(), "Please enter name to update", Toast.LENGTH_LONG).show();
                    }
                }


            }
        });


        binding.editMobileButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                if (!binding.editMobile.isEnabled()) {
                    binding.editMobile.setEnabled(true);
                    binding.editMobile.requestFocus();
                    binding.editMobile.setText("");
                    binding.editMobile.setHint("Enter new number");
                    return;
                } else {

                    if (!binding.editMobile.getText().toString().isEmpty()) {


                        contactViewModel.updateNumber(Constants.getUserID(getApplicationContext()), binding.editMobile.getText().toString(),
                                getApplicationContext());
                    } else {
                        Toast.makeText(getApplicationContext(), "Please enter name to update", Toast.LENGTH_LONG).show();
                    }
                }

            }
        });




        binding.editDobButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {

                    Calendar calendar = Calendar.getInstance();

                    int month = calendar.get(Calendar.MONTH);

                    int day = calendar.get(Calendar.DAY_OF_MONTH);

                    int year = calendar.get(Calendar.YEAR);









                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

                        DatePickerDialog.OnDateSetListener onDateSetListener = new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {


                                binding.editDob.setText(String.valueOf(dayOfMonth)+"/"+String.valueOf(month+1)+"/"+String.valueOf(year));


                                if (!binding.editName.getText().toString().isEmpty()) {

                                    contactViewModel.updateDOB(Constants.getUserID(getApplicationContext()), binding.editDob.getText().toString(),
                                            getApplicationContext());
                                }

                            }
                        };

                        DatePickerDialog datePickerDialog = new DatePickerDialog(v.getContext(),onDateSetListener,year,month,day);

                        datePickerDialog.show();


                    }

                }else {


                    if (!binding.editDob.isEnabled()) {
                        binding.editDob.setEnabled(true);
                        binding.editDob.requestFocus();
                        binding.editDob.setText("");
                        binding.editDob.setHint("Enter new number");
                        return;
                    } else {

                        if (!binding.editDob.getText().toString().isEmpty()) {


                            contactViewModel.updateDOB(Constants.getUserID(getApplicationContext()), binding.editDob.getText().toString(),
                                    getApplicationContext());
                        } else {
                            Toast.makeText(getApplicationContext(), "Please enter name to update", Toast.LENGTH_LONG).show();
                        }
                    }

                }
            }
        });



    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.profile_menu,menu);

        MenuItem settingsMenu = menu.findItem(R.id.setting);
        MenuItem blockMenu = menu.findItem(R.id.block);
        MenuItem shareMenu = menu.findItem(R.id.share);
        MenuItem callMenu = menu.findItem(R.id.call);


        if (isCurrentUser){

            settingsMenu.setVisible(false);
            callMenu.setVisible(false);
            blockMenu.setVisible(false);

        }

        if (isContactBlock){
            blockMenu.setTitle("Unblock");
            shareMenu.setVisible(false);
            callMenu.setVisible(false);

        }
        return super.onCreateOptionsMenu(menu);


    }


    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        if (item.getItemId()==R.id.setting){


            startActivity(new Intent(this, SettingActivity.class));

        }else if(item.getItemId()==R.id.call){

            calllUser(userMobileNumber);


        }else if (item.getItemId()==R.id.share){
            shareContact(userMobileNumber);

        }else if(item.getItemId()==R.id.block){
            contactViewModel.addBlockUser(getApplicationContext(),Constants.getUserID(getApplicationContext()),userId);

        }

        return super.onOptionsItemSelected(item);

    }

    private void calllUser(String userMobileNumber) {

        if (userMobileNumber!=null) {

            Dexter.withContext(getApplicationContext())
                    .withPermission(Manifest.permission.CALL_PHONE)
                    .withListener(new PermissionListener() {
                        @Override
                        public void onPermissionGranted(PermissionGrantedResponse permissionGrantedResponse) {


                            String number = "tel:" + userMobileNumber;

                            if (TextUtils.isDigitsOnly(userMobileNumber)) {
                                Intent intent = new Intent(Intent.ACTION_DIAL);
                                intent.setData(Uri.parse(number));
                                startActivity(intent);
                            } else {
                                Toast.makeText(getApplicationContext(), "There" +
                                        "s something wrong with user number!" + " Please try later", Toast.LENGTH_LONG).show();
                            }


                        }

                        @Override
                        public void onPermissionDenied(PermissionDeniedResponse permissionDeniedResponse) {

                        }

                        @Override
                        public void onPermissionRationaleShouldBeShown(PermissionRequest permissionRequest, PermissionToken permissionToken) {

                        }
                    }).check();
        } else {

            Toast.makeText(getApplicationContext(), "User number not available! please try later", Toast.LENGTH_LONG).show();
        }

    }


    private void shareContact(String userNumber){

        if (userNumber!=null) {
            Intent intent = new Intent(Intent.ACTION_SEND);
            String shareMessage = "Hey i found this contact! very informative and useful, You can connect with him too " + userNumber;

            intent.setType("text/plain");
            intent.putExtra(Intent.EXTRA_SUBJECT, "Share Contact");
            intent.putExtra(Intent.EXTRA_TEXT, shareMessage);
            startActivity(Intent.createChooser(intent, "Share Contact"));

        }else {
            Toast.makeText(getApplicationContext(), "User number not available! please try later", Toast.LENGTH_LONG).show();

        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);



        Uri uri = data.getData();
        binding.profileImage.setImageURI(uri);



        File file = ImagePicker.Companion.getFile(data);


        uploadProfileImage(file);




    }

    private void uploadProfileImage(File file) {



        contactViewModel.changeProfileImage(Constants.getUserID(getApplicationContext()),getApplicationContext(),file);





    }


}