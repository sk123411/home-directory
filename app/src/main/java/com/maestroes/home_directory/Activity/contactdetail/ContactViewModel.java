package com.maestroes.home_directory.Activity.contactdetail;

import android.content.Context;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.maestroes.home_directory.Model.DeleteUserResponse;
import com.maestroes.home_directory.Model.ListProfileResponse;
import com.maestroes.home_directory.Model.ProfileResponse;
import com.maestroes.home_directory.helper.ApiResponse;

import java.io.File;
import java.util.HashMap;

public class ContactViewModel extends ViewModel{


    private ContactImplementation contactImplementation;

    private MutableLiveData<ProfileResponse> profileResponseMutableLiveData=  new MutableLiveData<>();
    private MutableLiveData<ListProfileResponse> listProfileResponseMutableLiveData=  new MutableLiveData<>();
    private MutableLiveData<DeleteUserResponse> deleteUserResponseMutableLiveData=  new MutableLiveData<>();

    private MutableLiveData<ProfileEvent> profileEventMutableLiveData=  new MutableLiveData<>();







    public void getProfile(String userID, Context context) {

        profileEventMutableLiveData.setValue(ProfileEvent.LOADING);

        HashMap<String,String> map = new HashMap<>();

        contactImplementation = new ContactImplementation(context);
        map.put("userID",userID);


        contactImplementation.getProfile("update_profile", map, new ApiResponse<ProfileResponse>() {
            @Override
            public void data(ProfileResponse data) {

                profileEventMutableLiveData.setValue(ProfileEvent.DATA);

                profileResponseMutableLiveData.setValue(data);
            }

            @Override
            public void Error(String message) {
                profileEventMutableLiveData.setValue(ProfileEvent.ERROR);

            }
        });



    }



    public void changeProfileImage(String userID, Context context, File file) {

        profileEventMutableLiveData.setValue(ProfileEvent.LOADING);

        HashMap<String,String> map = new HashMap<>();

        contactImplementation = new ContactImplementation(context);
        map.put("userID",userID);


        contactImplementation.changeProfile("update_profile", map, file,new ApiResponse<ProfileResponse>() {
            @Override
            public void data(ProfileResponse data) {

                profileEventMutableLiveData.setValue(ProfileEvent.DATA);

                profileResponseMutableLiveData.setValue(data);
            }

            @Override
            public void Error(String message) {
                profileEventMutableLiveData.setValue(ProfileEvent.ERROR);

            }
        });



    }

    public void updateName(String userID, String name, Context context) {


        profileEventMutableLiveData.setValue(ProfileEvent.LOADING);

        HashMap<String,String> map = new HashMap<>();

        contactImplementation = new ContactImplementation(context);
        map.put("userID",userID);
        map.put("name",name);


        contactImplementation.updateName("update_profile", map, new ApiResponse<ProfileResponse>() {
            @Override
            public void data(ProfileResponse data) {

                profileEventMutableLiveData.setValue(ProfileEvent.DATA);

                profileResponseMutableLiveData.setValue(data);
            }

            @Override
            public void Error(String message) {
                profileEventMutableLiveData.setValue(ProfileEvent.ERROR);

            }
        });




    }

    public void updateEmail(String userID, String email, Context context) {
        profileEventMutableLiveData.setValue(ProfileEvent.LOADING);

        HashMap<String,String> map = new HashMap<>();

        contactImplementation = new ContactImplementation(context);
        map.put("userID",userID);
        map.put("email",email);


        contactImplementation.updateEmail("update_profile", map, new ApiResponse<ProfileResponse>() {
            @Override
            public void data(ProfileResponse data) {

                profileEventMutableLiveData.setValue(ProfileEvent.DATA);

                profileResponseMutableLiveData.setValue(data);
            }

            @Override
            public void Error(String message) {
                profileEventMutableLiveData.setValue(ProfileEvent.ERROR);

            }
        });
    }

    public void updateDOB(String userID, String dob, Context context) {
        profileEventMutableLiveData.setValue(ProfileEvent.LOADING);

        HashMap<String,String> map = new HashMap<>();

        contactImplementation = new ContactImplementation(context);
        map.put("userID",userID);
        map.put("dob",dob);


        contactImplementation.updateDOB("update_profile", map, new ApiResponse<ProfileResponse>() {
            @Override
            public void data(ProfileResponse data) {


                if (data.getResult()) {
                    profileEventMutableLiveData.setValue(ProfileEvent.DATA);

                    profileResponseMutableLiveData.setValue(data);

                }else {

                    profileEventMutableLiveData.setValue(ProfileEvent.ERROR);

                }
                }

            @Override
            public void Error(String message) {
                profileEventMutableLiveData.setValue(ProfileEvent.ERROR);

            }
        });
    }

    public void updatePassword(String userID, String password, Context context) {

        profileEventMutableLiveData.setValue(ProfileEvent.LOADING);

        HashMap<String,String> map = new HashMap<>();

        contactImplementation = new ContactImplementation(context);
        map.put("userID",userID);
        map.put("password",password);


        contactImplementation.updatePassword("update_profile", map, new ApiResponse<ProfileResponse>() {
            @Override
            public void data(ProfileResponse data) {

                profileEventMutableLiveData.setValue(ProfileEvent.DATA);

                profileResponseMutableLiveData.setValue(data);
            }

            @Override
            public void Error(String message) {
                profileEventMutableLiveData.setValue(ProfileEvent.ERROR);

            }
        });


    }

    public void updateNumber(String userID, String number, Context context) {
        profileEventMutableLiveData.setValue(ProfileEvent.LOADING);

        HashMap<String,String> map = new HashMap<>();

        contactImplementation = new ContactImplementation(context);
        map.put("userID",userID);
        map.put("number",number);


        contactImplementation.updateNumber("update_profile", map, new ApiResponse<ProfileResponse>() {
            @Override
            public void data(ProfileResponse data) {

                profileEventMutableLiveData.setValue(ProfileEvent.DATA);

                profileResponseMutableLiveData.setValue(data);
            }

            @Override
            public void Error(String message) {
                profileEventMutableLiveData.setValue(ProfileEvent.ERROR);

            }
        });
    }





    public void getListProfiles(Context context,String userID) {
        profileEventMutableLiveData.setValue(ProfileEvent.LOADING);

        HashMap<String,String> map = new HashMap<>();
        map.put("user_id",userID);

        contactImplementation = new ContactImplementation(context);


        contactImplementation.getListProfiles("show_contact", map, new ApiResponse<ListProfileResponse>() {
            @Override
            public void data(ListProfileResponse data) {

                profileEventMutableLiveData.setValue(ProfileEvent.DATA);

                listProfileResponseMutableLiveData.setValue(data);
            }

            @Override
            public void Error(String message) {
                profileEventMutableLiveData.setValue(ProfileEvent.ERROR);

            }
        });
    }





    public void deleteUser(Context context,String userID,String deletedUserID) {
        profileEventMutableLiveData.setValue(ProfileEvent.LOADING);

        HashMap<String,String> map = new HashMap<>();

        contactImplementation = new ContactImplementation(context);
        map.put("user_id",userID);
        map.put("delete_user_id",deletedUserID);

        contactImplementation.deleteUser("add_to_delete_user", map, new ApiResponse<DeleteUserResponse>() {
            @Override
            public void data(DeleteUserResponse data) {

                profileEventMutableLiveData.setValue(ProfileEvent.DATA);

                deleteUserResponseMutableLiveData.setValue(data);
            }

            @Override
            public void Error(String message) {
                profileEventMutableLiveData.setValue(ProfileEvent.ERROR);

            }
        });
    }




    public void addBlockUser(Context context,String userID,String blockUserID) {
        profileEventMutableLiveData.setValue(ProfileEvent.LOADING);

        HashMap<String,String> map = new HashMap<>();

        contactImplementation = new ContactImplementation(context);
        map.put("user_id",userID);
        map.put("block_user_id",blockUserID);

        contactImplementation.addBlockUser("add_to_block_contact", map, new ApiResponse<DeleteUserResponse>() {
            @Override
            public void data(DeleteUserResponse data) {

                profileEventMutableLiveData.setValue(ProfileEvent.DATA);

                deleteUserResponseMutableLiveData.setValue(data);
            }

            @Override
            public void Error(String message) {
                profileEventMutableLiveData.setValue(ProfileEvent.ERROR);

            }
        });
    }

    public enum  ProfileEvent{

         DATA,ERROR,LOADING
    }








    public LiveData<ProfileResponse> getResponse() {
        return profileResponseMutableLiveData;
    }

    public LiveData<ListProfileResponse> getListResponse() {
        return listProfileResponseMutableLiveData;
    }
    public LiveData<DeleteUserResponse> getDeletedUserLiveData() {
        return deleteUserResponseMutableLiveData;
    }


    public LiveData<ProfileEvent> getLoginEvent() {
        return profileEventMutableLiveData;
    }






}
