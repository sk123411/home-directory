package com.maestroes.home_directory.Activity.login;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.maestroes.home_directory.Activity.ContactActivity;
import com.maestroes.home_directory.Activity.contactdetail.ContactViewModel;
import com.maestroes.home_directory.Activity.signup.SignUpActivity;
import com.maestroes.home_directory.Model.ProfileResponse;
import com.maestroes.home_directory.Model.SignUpResponse;
import com.maestroes.home_directory.databinding.ActivityLoginBinding;
import com.maestroes.home_directory.databinding.ActivitySignUpBinding;
import com.maestroes.home_directory.databinding.ForgetPasswordLayoutBinding;
import com.maestroes.home_directory.helper.Constants;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;

import io.paperdb.Paper;

public class LoginActivity extends AppCompatActivity {


    private static final int RC_SIGN_IN = 789;
    private com.maestroes.home_directory.databinding.ActivityLoginBinding binding;
    private LoginViewModel loginViewModel;
    private ContactViewModel contactViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityLoginBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        loginViewModel = ViewModelProviders.of(this).get(LoginViewModel.class);
        contactViewModel = ViewModelProviders.of(this).get(ContactViewModel.class);


        loginViewModel.getResponse().observe(this, new Observer<SignUpResponse>() {
            @Override
            public void onChanged(SignUpResponse signUpResponse) {


                if (signUpResponse!=null) {
                    Toast.makeText(getApplicationContext(), "login successfull", Toast.LENGTH_LONG).show();

                    if (signUpResponse.getResult()) {
                        Paper.init(getApplicationContext());

                        Paper.book().write(Constants.userID, signUpResponse.getData().getUserID());
                        startActivity(new Intent(getApplicationContext(), ContactActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                    }
                    //

                }


            }
        });




        contactViewModel.getResponse().observe(this, new Observer<ProfileResponse>() {
            @Override
            public void onChanged(ProfileResponse profileResponse) {

                if (profileResponse!=null){
                    Toast.makeText(getApplicationContext(), "Profile updated", Toast.LENGTH_LONG).show();
                }
            }
        });


        loginViewModel.getSocialSignUpResponse().observe(this, new Observer<ProfileResponse>() {
            @Override
            public void onChanged(ProfileResponse signUpResponse) {


                if (signUpResponse!=null) {
                    Toast.makeText(getApplicationContext(), "login successfull", Toast.LENGTH_LONG).show();

                    if (signUpResponse.getResult()) {

                        if (signUpResponse.getData()!=null) {

                            contactViewModel.updateName(signUpResponse.getData().getUserID(),"Your name",getApplicationContext());

                            Paper.init(getApplicationContext());

                            Paper.book().write(Constants.userID, signUpResponse.getData().getUserID());
                            startActivity(new Intent(getApplicationContext(), ContactActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                        }

                        }

                }


            }
        });




        binding.google.setOnClickListener(

                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        loginViewModel.googleSignIn(LoginActivity.this,RC_SIGN_IN);
                    }
                }
        );


        loginViewModel.getLoginEvent().observe(this, new Observer<LoginViewModel.LoginEvent>() {
            @Override
            public void onChanged(LoginViewModel.LoginEvent signUpEvent) {

                switch (signUpEvent){

                    case DATA:
                        binding.progressBar.setVisibility(View.GONE);
                        break;
                    case ERROR:
                        Toast.makeText(getApplicationContext(),"Servor error occured, Please try later",Toast.LENGTH_LONG).show();
                        binding.progressBar.setVisibility(View.GONE);
                        break;
                    case LOADING:
                        binding.progressBar.setVisibility(View.VISIBLE);
                        break;



                }

            }
        });


        binding.buttonLogin.setOnClickListener(

                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        if (!binding.emailEdit.getText().toString().isEmpty()&&
                                !binding.passwordEdit.getText().toString().isEmpty()){

                            loginViewModel.login(
                                    binding.emailEdit.getText().toString(),
                                    binding.passwordEdit.getText().toString(),
                                    getApplicationContext()
                            );


                       }else {


                            Toast.makeText(getApplicationContext(),"Please fill all the fields",Toast.LENGTH_LONG).show();

                        }

                    }
                }

        );

        binding.signupButton.setOnClickListener(

                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        startActivity(new Intent(LoginActivity.this,SignUpActivity.class));

                        }


                }

        );


        binding.loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                loginViewModel.facebookSignIn(LoginActivity.this);

            }
        });









        loginViewModel.getForgetPassword().observe(LoginActivity.this, new Observer<String>() {
            @Override
            public void onChanged(String s) {




                Toast.makeText(getApplicationContext(),s,Toast.LENGTH_LONG).show();


            }
        });


        binding.forgetPasswordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                openForgetPasswordDialog();
            }
        });

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = task.getResult(ApiException.class);
                firebaseAuthWithGoogle(account.getIdToken());
            } catch (ApiException e) {

                Log.d("TTTTTTTTT" , "::" + e.getMessage());
                Log.d("TTTTTTTTT" , "::" + e.getLocalizedMessage());
                Log.d("TTTTTTTTT" , "::" + e.getStatusCode());

                // Google Sign In failed, update UI appropriately
               // Log.w(TAG, "Google sign in failed", e);
            }
        }else {

            loginViewModel.callbackManager.onActivityResult(requestCode, resultCode, data);

        }
    }


    private void firebaseAuthWithGoogle(String idToken) {
        AuthCredential credential = GoogleAuthProvider.getCredential(idToken, null);
        FirebaseAuth mAuth = FirebaseAuth.getInstance();

        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                          //  Log.d(TAG, "signInWithCredential:success");
                            FirebaseUser user = mAuth.getCurrentUser();

                            Log.d("XXXXXXXXX", "::"+user.getEmail());
                            updateUI(user);
                        } else {
                            Log.d("XXXXXXXXX", "signInWithCredential:failure", task.getException());
                            //updateUI(null);


                        }
                    }
                });
    }

    private void updateUI(FirebaseUser user) {

        Paper.init(this);

        loginViewModel.socialSignIn(user.getDisplayName()==null?user.getEmail().substring(0,4):user.getDisplayName()
        ,user.getEmail(),user.getProviderId(),"Google",getApplicationContext());






    }




    private void openForgetPasswordDialog(){


        ForgetPasswordLayoutBinding binding = ForgetPasswordLayoutBinding.inflate(getLayoutInflater());

        BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(LoginActivity.this);

        bottomSheetDialog.setContentView(binding.getRoot());


        bottomSheetDialog.show();





        binding.saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (!binding.usernameEdit.getText().toString().isEmpty()){



                    if (binding.usernameEdit.getText().toString().contains("@")) {

                        loginViewModel.sendPasswordReset(getApplicationContext(), binding.usernameEdit.getText().toString());
                    }else {

                        Toast.makeText(getApplicationContext(),"Please input corrent email address",Toast.LENGTH_LONG).show();
                    }
                }


                bottomSheetDialog.dismiss();

            }
        });








    }



}
