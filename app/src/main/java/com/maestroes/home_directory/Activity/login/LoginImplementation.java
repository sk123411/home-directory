package com.maestroes.home_directory.Activity.login;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

import com.androidnetworking.AndroidNetworking;
import com.maestroes.home_directory.Model.ProfileResponse;
import com.maestroes.home_directory.Model.SignUpResponse;
import com.maestroes.home_directory.R;
import com.maestroes.home_directory.helper.ApiResponse;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.rx2androidnetworking.Rx2AndroidNetworking;

import org.json.JSONObject;

import java.util.HashMap;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class LoginImplementation implements Login {
        private Context application;

      public LoginImplementation(Context a){
          this.application = a;
      }


    @Override
    public void login(String control, HashMap<String,String> data, ApiResponse<SignUpResponse> apiResponse) {
        AndroidNetworking.initialize(application);


        Rx2AndroidNetworking.post("https://maestros.co.in/Home_diractory/appservice/process.php")
                .addBodyParameter("control",control)
                .addBodyParameter("email",data.get("email"))
                .addBodyParameter("password",data.get("password"))
                .build().

                getObjectObservable(SignUpResponse.class).observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io()).subscribe(new Observer<SignUpResponse>() {
            @Override
            public void onSubscribe(@NonNull Disposable d) {

            }

            @Override
            public void onNext(@NonNull SignUpResponse signUpResponse) {
                apiResponse.data(signUpResponse);


            }

            @Override
            public void onError(@NonNull Throwable e) {

                Log.d("DATAAAAAAAAAAAAAA", ":"+e.getMessage());
                apiResponse.Error(e.getMessage());

            }

            @Override
            public void onComplete() {

            }
        });


    }

    @Override
    public Intent googleSignIn(AppCompatActivity appCompatActivity) {

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(appCompatActivity.getResources().getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        return GoogleSignIn.getClient(appCompatActivity, gso).getSignInIntent();



      }

    @Override
    public void SignInSocial(String control, HashMap<String, String> data, ApiResponse<ProfileResponse> apiResponse) {


        AndroidNetworking.initialize(application);


        Rx2AndroidNetworking.post("https://maestros.co.in/Home_diractory/appservice/process.php")
                .addBodyParameter("control","social_signup")
                .addBodyParameter("auth_id",data.get("auth_id"))
                .addBodyParameter("auth_provider",data.get("auth_provider"))
                .addBodyParameter("name",data.get("name"))
                .addBodyParameter("email",data.get("email"))
                .addBodyParameter("fname",data.get("name"))

                .build().

                getObjectObservable(ProfileResponse.class).observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io()).subscribe(new Observer<ProfileResponse>() {
            @Override
            public void onSubscribe(@NonNull Disposable d) {

            }

            @Override
            public void onNext(@NonNull ProfileResponse signUpResponse) {
                apiResponse.data(signUpResponse);
            }

            @Override
            public void onError(@NonNull Throwable e) {
                apiResponse.Error(e.getMessage());

            }

            @Override
            public void onComplete() {

            }
        });


    }

    @Override
    public void forgetPassword(String control, HashMap<String, String> data, ApiResponse<JSONObject> apiResponse) {
        AndroidNetworking.initialize(application);


        Rx2AndroidNetworking.post("https://maestros.co.in/Home_diractory/appservice/process.php")
                .addBodyParameter("control","forget_password")
                .addBodyParameter("email",data.get("email"))
                .build().

                getJSONObjectObservable().observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io()).subscribe(new Observer<JSONObject>() {
            @Override
            public void onSubscribe(@NonNull Disposable d) {

            }

            @Override
            public void onNext(@NonNull JSONObject signUpResponse) {
                apiResponse.data(signUpResponse);
            }

            @Override
            public void onError(@NonNull Throwable e) {
                apiResponse.Error(e.getMessage());

            }

            @Override
            public void onComplete() {

            }
        });

    }



}
