package com.maestroes.home_directory.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.navigation.NavigationView;
import com.maestroes.home_directory.Activity.contactdetail.ContactDetailsActivity;
import com.maestroes.home_directory.Activity.contactdetail.ContactViewModel;
import com.maestroes.home_directory.Activity.login.LoginActivity;
import com.maestroes.home_directory.Adapter.ContactAdapter;
import com.maestroes.home_directory.Model.ContactModel;
import com.maestroes.home_directory.Model.ListProfileResponse;
import com.maestroes.home_directory.Model.ProfileResponse;
import com.maestroes.home_directory.Model.SignUpResponse;
import com.maestroes.home_directory.R;
import com.maestroes.home_directory.databinding.ActivityContactBinding;
import com.maestroes.home_directory.helper.Constants;
import com.google.firebase.auth.FirebaseAuth;

import java.util.ArrayList;

import io.paperdb.Paper;

public class ContactActivity extends AppCompatActivity {

    RecyclerView recycle_view;
    Toolbar toolbar;
    private ContactViewModel contactViewModel;
    private ProgressBar progressBar;
    private EditText searchEdit;
    private ActivityContactBinding binding;
    public Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityContactBinding.inflate(getLayoutInflater());

        setContentView(binding.getRoot());
        recycle_view=findViewById(R.id.recycle_view);
        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Contacts");
        progressBar = findViewById(R.id.progressBar);
        searchEdit = findViewById(R.id.searchEdit);
        Paper.init(getApplicationContext());
        contactViewModel = ViewModelProviders.of(this).get(ContactViewModel.class);
        contactViewModel.getListProfiles(getApplicationContext(),Constants.getUserID(getApplicationContext()));

        recycle_view.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));



        ArrayList<ListProfileResponse.Data> savedContactUser = Paper.book().read(Constants.CONTACTS);




        String userId = Constants.getUserID(getApplicationContext());


        if (userId!=null){
            contactViewModel.getProfile(userId, getApplicationContext());


        }


        NavigationView navigationView = findViewById(R.id.nav_view);
        DrawerLayout drawerLayout = findViewById(R.id.drawerLayout);




        ImageView menu = findViewById(R.id.menuButton);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);

        binding.contentMain.contentSecondary.menuButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Toast.makeText(getApplicationContext(), "ccccc", Toast.LENGTH_LONG).show();

                binding.contentMain.drawerLayout.openDrawer(GravityCompat.START);


            }
        });








        contactViewModel.getResponse().observe(this, new Observer<ProfileResponse>() {
            @Override
            public void onChanged(ProfileResponse signUpResponse) {


                if (signUpResponse != null) {



                    View view = navigationView.getHeaderView(0);

                    TextView headerTitle = view.findViewById(R.id.nameTextView);
                    TextView headerDes = view.findViewById(R.id.subTextView);


                    headerTitle.setText("Logged in as: " +signUpResponse.getData().getFname());
                    headerDes.setText("Number: " + signUpResponse.getData().getMobile());



                }


            }
        });






        searchEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {


                if (!s.toString().isEmpty()&&s.length()>0) {

                    ArrayList<ListProfileResponse.Data> filterList = new ArrayList<>();

                    if (savedContactUser!=null) {

                        for (ListProfileResponse.Data search : savedContactUser) {

                            if (search.getEmail().toLowerCase().contains(s.toString().toLowerCase())) {
                                filterList.add(search);

                            }


                        }


                        recycle_view.setAdapter(new ContactAdapter(filterList, ContactActivity.this));

                    }
                }else {
                    recycle_view.setAdapter(new ContactAdapter(savedContactUser, ContactActivity.this));


                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });







        setSupportActionBar(toolbar);



        contactViewModel.getListResponse().observe(this, new Observer<ListProfileResponse>() {
            @Override
            public void onChanged(ListProfileResponse signUpResponse) {


                if (signUpResponse != null) {
                    Toast.makeText(getApplicationContext(), "Profile updated", Toast.LENGTH_LONG).show();
                    ArrayList<ListProfileResponse.Data> profileResponses = new ArrayList<>();

                    for (ListProfileResponse.Data data:signUpResponse.getDataList()){


                        if (!data.getDeletedStatus()){
                            profileResponses.add(data);
                        }

                    }

                    Paper.book().write(Constants.CONTACTS,profileResponses);

                    recycle_view.setAdapter(new ContactAdapter(profileResponses,ContactActivity.this));


                    }
                }



        });



        contactViewModel.getLoginEvent().observe(this, new Observer<ContactViewModel.ProfileEvent>() {
            @Override
            public void onChanged(ContactViewModel.ProfileEvent profileEvent) {

                switch (profileEvent) {

                    case DATA:
                        progressBar.setVisibility(View.GONE);
                        break;
                    case ERROR:
                        Toast.makeText(getApplicationContext(), "Servor error occured, Please try later", Toast.LENGTH_LONG).show();
                        progressBar.setVisibility(View.GONE);
                        break;
                    case LOADING:
                       progressBar.setVisibility(View.VISIBLE);
                        break;

                }

            }
        });














    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.main_menu,menu);
        return super.onCreateOptionsMenu(menu);


    }


    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        if (item.getItemId()==R.id.logout){

                Paper.init(this);
                Paper.book().delete(Constants.userID);
                Paper.book().delete(Constants.isSocialSignUp);
                FirebaseAuth.getInstance().signOut();

                startActivity(new Intent(this, LoginActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));

        }else
        if (item.getItemId()==R.id.profile){



            startActivity(new Intent(this, ContactDetailsActivity.class)
                    .putExtra("id", Constants.getUserID(getApplicationContext())));


        }

        return super.onOptionsItemSelected(item);

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();

        finishAffinity();


    }
}