package com.maestroes.home_directory.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.maestroes.home_directory.Activity.login.LoginActivity;
import com.maestroes.home_directory.R;
import com.maestroes.home_directory.helper.Constants;

import io.paperdb.Paper;

public class SpalshActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spalsh);


        Paper.init(this);

        String isLoggedIn = Constants.getUserID(this);


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                if (isLoggedIn==null) {

                    startActivity(new Intent(getApplicationContext(), LoginActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));

                }else {
                    startActivity(new Intent(getApplicationContext(), ContactActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));

                }
                }
        }, 2000);
    }
}