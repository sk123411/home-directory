package com.maestroes.home_directory.Activity.contactdetail;

import android.content.Context;
import android.util.Log;

import com.androidnetworking.AndroidNetworking;
import com.maestroes.home_directory.Model.DeleteUserResponse;
import com.maestroes.home_directory.Model.ListProfileResponse;
import com.maestroes.home_directory.Model.ProfileResponse;
import com.maestroes.home_directory.helper.ApiResponse;
import com.rx2androidnetworking.Rx2AndroidNetworking;

import java.io.File;
import java.util.HashMap;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class ContactImplementation implements Contact {
        private Context application;

      public ContactImplementation(Context a){
          this.application = a;
      }


    @Override
    public void getProfile(String control, HashMap<String,String> data, ApiResponse<ProfileResponse> apiResponse) {
        AndroidNetworking.initialize(application);


        Rx2AndroidNetworking.post("https://maestros.co.in/Home_diractory/appservice/process.php")
                .addBodyParameter("control","update_profile")
                .addBodyParameter("userID",data.get("userID"))
                .build().

                getObjectObservable(ProfileResponse.class).observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io()).subscribe(new Observer<ProfileResponse>() {
            @Override
            public void onSubscribe(@NonNull Disposable d) {

            }

            @Override
            public void onNext(@NonNull ProfileResponse signUpResponse) {
                apiResponse.data(signUpResponse);
            }

            @Override
            public void onError(@NonNull Throwable e) {
                apiResponse.Error(e.getMessage());

            }

            @Override
            public void onComplete() {

            }
        });


    }

    @Override
    public void changeProfile(String control, HashMap<String, String> data, File file, ApiResponse<ProfileResponse> apiResponse) {
        AndroidNetworking.initialize(application);


        Rx2AndroidNetworking.upload("https://maestros.co.in/Home_diractory/appservice/process.php")
                .addMultipartParameter("control","update_profile")
                .addMultipartParameter("userID",data.get("userID"))
                .addMultipartFile("image",file)
                .build().

                getObjectObservable(ProfileResponse.class).observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io()).subscribe(new Observer<ProfileResponse>() {
            @Override
            public void onSubscribe(@NonNull Disposable d) {

            }

            @Override
            public void onNext(@NonNull ProfileResponse signUpResponse) {
                apiResponse.data(signUpResponse);
            }

            @Override
            public void onError(@NonNull Throwable e) {
                apiResponse.Error(e.getMessage());

            }

            @Override
            public void onComplete() {

            }
        });
    }

    @Override
    public void updateName(String control, HashMap<String, String> data, ApiResponse<ProfileResponse> apiResponse) {
        AndroidNetworking.initialize(application);


        Rx2AndroidNetworking.post("https://maestros.co.in/Home_diractory/appservice/process.php")
                .addBodyParameter("control","update_profile")
                .addBodyParameter("userID",data.get("userID"))
                .addBodyParameter("fname",data.get("name"))

                .build().

                getObjectObservable(ProfileResponse.class).observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io()).subscribe(new Observer<ProfileResponse>() {
            @Override
            public void onSubscribe(@NonNull Disposable d) {

            }

            @Override
            public void onNext(@NonNull ProfileResponse signUpResponse) {
                apiResponse.data(signUpResponse);
            }

            @Override
            public void onError(@NonNull Throwable e) {
                apiResponse.Error(e.getMessage());

            }

            @Override
            public void onComplete() {

            }
        });
    }

    @Override
    public void updateEmail(String control, HashMap<String, String> data, ApiResponse<ProfileResponse> apiResponse) {
        AndroidNetworking.initialize(application);


        Rx2AndroidNetworking.post("https://maestros.co.in/Home_diractory/appservice/process.php")
                .addBodyParameter("control","update_profile")
                .addBodyParameter("userID",data.get("userID"))
                .addBodyParameter("email",data.get("email"))

                .build().

                getObjectObservable(ProfileResponse.class).observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io()).subscribe(new Observer<ProfileResponse>() {
            @Override
            public void onSubscribe(@NonNull Disposable d) {

            }

            @Override
            public void onNext(@NonNull ProfileResponse signUpResponse) {
                apiResponse.data(signUpResponse);
            }

            @Override
            public void onError(@NonNull Throwable e) {
                apiResponse.Error(e.getMessage());

            }

            @Override
            public void onComplete() {

            }
        });
    }

    @Override
    public void updateDOB(String control, HashMap<String, String> data, ApiResponse<ProfileResponse> apiResponse) {


        AndroidNetworking.initialize(application);


        Rx2AndroidNetworking.post("https://maestros.co.in/Home_diractory/appservice/process.php")
                .addBodyParameter("control","update_profile")
                .addBodyParameter("userID",data.get("userID"))
                .addBodyParameter("dob",data.get("dob"))

                .build().

                getObjectObservable(ProfileResponse.class).observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io()).subscribe(new Observer<ProfileResponse>() {
            @Override
            public void onSubscribe(@NonNull Disposable d) {

            }

            @Override
            public void onNext(@NonNull ProfileResponse signUpResponse) {
                apiResponse.data(signUpResponse);
            }

            @Override
            public void onError(@NonNull Throwable e) {
                apiResponse.Error(e.getMessage());

            }

            @Override
            public void onComplete() {

            }
        });
    }

    @Override
    public void updatePassword(String control, HashMap<String, String> data, ApiResponse<ProfileResponse> apiResponse) {
        AndroidNetworking.initialize(application);


        Rx2AndroidNetworking.post("https://maestros.co.in/Home_diractory/appservice/process.php")
                .addBodyParameter("control","update_profile")
                .addBodyParameter("userID",data.get("userID"))
                .addBodyParameter("password",data.get("password"))

                .build().

                getObjectObservable(ProfileResponse.class).observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io()).subscribe(new Observer<ProfileResponse>() {
            @Override
            public void onSubscribe(@NonNull Disposable d) {

            }

            @Override
            public void onNext(@NonNull ProfileResponse signUpResponse) {
                apiResponse.data(signUpResponse);
            }

            @Override
            public void onError(@NonNull Throwable e) {
                apiResponse.Error(e.getMessage());

            }

            @Override
            public void onComplete() {

            }
        });
    }

    @Override
    public void updateNumber(String control, HashMap<String, String> data, ApiResponse<ProfileResponse> apiResponse) {

        AndroidNetworking.initialize(application);


        Rx2AndroidNetworking.post("https://maestros.co.in/Home_diractory/appservice/process.php")
                .addBodyParameter("control","update_profile")
                .addBodyParameter("userID",data.get("userID"))
                .addBodyParameter("mobile",data.get("number"))

                .build().

                getObjectObservable(ProfileResponse.class).observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io()).subscribe(new Observer<ProfileResponse>() {
            @Override
            public void onSubscribe(@NonNull Disposable d) {

            }

            @Override
            public void onNext(@NonNull ProfileResponse signUpResponse) {
                apiResponse.data(signUpResponse);
            }

            @Override
            public void onError(@NonNull Throwable e) {
                apiResponse.Error(e.getMessage());

            }

            @Override
            public void onComplete() {

            }
        });
    }

    @Override
    public void getListProfiles(String control, HashMap<String, String> data, ApiResponse<ListProfileResponse> listProfileResponseApiResponse) {
        AndroidNetworking.initialize(application);


        Rx2AndroidNetworking.post("https://maestros.co.in/Home_diractory/appservice/process.php")
                .addBodyParameter("control","show_contact")
                .addBodyParameter("user_id",data.get("user_id"))
                .build().
                getObjectObservable(ListProfileResponse.class).observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io()).subscribe(new Observer<ListProfileResponse>() {
            @Override
            public void onSubscribe(@NonNull Disposable d) {

            }

            @Override
            public void onNext(@NonNull ListProfileResponse signUpResponse) {
                listProfileResponseApiResponse.data(signUpResponse);

                Log.d("XXXXXXXXXXXXXx", ":::"+signUpResponse.getDataList());
            }

            @Override
            public void onError(@NonNull Throwable e) {
                listProfileResponseApiResponse.Error(e.getMessage());

            }

            @Override
            public void onComplete() {

            }
        });
    }

    @Override
    public void deleteUser(String control, HashMap<String, String> data, ApiResponse<DeleteUserResponse> apiResponse) {



        Rx2AndroidNetworking.post("https://maestros.co.in/Home_diractory/appservice/process.php")
                .addBodyParameter("control",control)
                .addBodyParameter("user_id",data.get("user_id"))
                .addBodyParameter("delete_user_id",data.get("delete_user_id"))

                .build().

                getObjectObservable(DeleteUserResponse.class).observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io()).subscribe(new Observer<DeleteUserResponse>() {
            @Override
            public void onSubscribe(@NonNull Disposable d) {

            }

            @Override
            public void onNext(@NonNull DeleteUserResponse signUpResponse) {
                apiResponse.data(signUpResponse);
            }

            @Override
            public void onError(@NonNull Throwable e) {
                apiResponse.Error(e.getMessage());

            }

            @Override
            public void onComplete() {

            }
        });




    }

    @Override
    public void addBlockUser(String control, HashMap<String, String> data, ApiResponse<DeleteUserResponse> apiResponse) {

        Log.d("MAPPPPPPPPPP", "::"+data.toString());
        Rx2AndroidNetworking.post("https://maestros.co.in/Home_diractory/appservice/process.php")
                .addBodyParameter("control","add_to_block_contact")
                .addBodyParameter("user_id",data.get("user_id"))
                .addBodyParameter("block_user_id",data.get("block_user_id"))

                .build().

                getObjectObservable(DeleteUserResponse.class).observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io()).subscribe(new Observer<DeleteUserResponse>() {
            @Override
            public void onSubscribe(@NonNull Disposable d) {

            }

            @Override
            public void onNext(@NonNull DeleteUserResponse signUpResponse) {
                apiResponse.data(signUpResponse);
            }

            @Override
            public void onError(@NonNull Throwable e) {
                apiResponse.Error(e.getMessage());

            }

            @Override
            public void onComplete() {

            }
        });



    }


}
