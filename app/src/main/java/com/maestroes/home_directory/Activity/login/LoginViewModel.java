package com.maestroes.home_directory.Activity.login;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.maestroes.home_directory.Activity.contactdetail.ContactImplementation;
import com.maestroes.home_directory.Activity.contactdetail.ContactViewModel;
import com.maestroes.home_directory.Model.ProfileResponse;
import com.maestroes.home_directory.Model.SignUpResponse;
import com.maestroes.home_directory.helper.ApiResponse;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;

import static com.facebook.FacebookSdk.getApplicationContext;

public class LoginViewModel extends ViewModel {


    private LoginImplementation loginImplementation;

    private MutableLiveData<SignUpResponse> signUpResponseMutableLiveData=  new MutableLiveData<>();
    private MutableLiveData<ProfileResponse> socialsignUpResponseMutableLiveData=  new MutableLiveData<>();

    private MutableLiveData<LoginEvent> loginEventMutableLiveData=  new MutableLiveData<>();
    private MutableLiveData<String> forgetPasswordResponse=  new MutableLiveData<>();

    public CallbackManager callbackManager;



     public enum  LoginEvent{

         DATA,ERROR,LOADING,SOCIALSIGNUP
    }





    public void login(String email,String password,Context context){

        loginEventMutableLiveData.setValue(LoginEvent.LOADING);

        HashMap<String,String> map = new HashMap<>();

        loginImplementation = new LoginImplementation(context);


        map.put("email",email);
        map.put("password",password);


        loginImplementation.login("login", map, new ApiResponse<SignUpResponse>() {
            @Override
            public void data(SignUpResponse data) {


                if (data.getResult()) {
                    loginEventMutableLiveData.setValue(LoginEvent.DATA);

                    signUpResponseMutableLiveData.setValue(data);
                }else {
                    Toast.makeText(context,""+data.getMessage(),Toast.LENGTH_LONG).show();

                    loginEventMutableLiveData.setValue(LoginEvent.ERROR);

                }
                }

          @Override
          public void Error(String message) {
              loginEventMutableLiveData.setValue(LoginEvent.ERROR);

          }
      });





    }



    public void googleSignIn(AppCompatActivity appCompatActivity, int code){

        loginImplementation = new LoginImplementation(appCompatActivity);

        Intent intent = loginImplementation.googleSignIn(appCompatActivity);

        appCompatActivity.startActivityForResult(intent,code);


    }






    public void socialSignIn(String name, String email, String authID, String authProvider, Context context){

        loginEventMutableLiveData.setValue(LoginViewModel.LoginEvent.LOADING);

        HashMap<String,String> map = new HashMap<>();

        loginImplementation = new LoginImplementation(context);

        map.put("name",name);
        map.put("fname",name);

        map.put("auth_id",authID);
        map.put("email",email);
        map.put("auth_provider",authProvider);

        loginImplementation.SignInSocial("social_signup", map, new ApiResponse<ProfileResponse>() {
            @Override
            public void data(ProfileResponse data) {

                if (data.getResult()) {


                    Log.d("PRRRRRRRRRRRRRRR", ":::" +data);
                    loginEventMutableLiveData.setValue(LoginEvent.SOCIALSIGNUP);

                    socialsignUpResponseMutableLiveData.setValue(data);
                }else {
                    Toast.makeText(context,""+data.getMessage(),Toast.LENGTH_LONG).show();

                    loginEventMutableLiveData.setValue(LoginEvent.ERROR);

                }

            }

            @Override
            public void Error(String message) {
                loginEventMutableLiveData.setValue(LoginViewModel.LoginEvent.ERROR);

            }
        });





    }




    public void facebookSignIn(Activity activity){



        // Initialize Facebook Login button
        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().logInWithReadPermissions(activity, Arrays.asList("email","public_profile"));
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.d("TAG", "facebook:onSuccess:" + loginResult);
                handleFacebookAccessToken(loginResult.getAccessToken(),activity);
            }

            @Override
            public void onCancel() {
                Log.d("TAG", "facebook:onCancel");
            }

            @Override
            public void onError(FacebookException error) {
                Log.d("TAG", "facebook:onError", error);
            }
        });
    }



    void sendPasswordReset(Context context,String email){

        loginEventMutableLiveData.setValue(LoginViewModel.LoginEvent.LOADING);

        HashMap<String,String> map = new HashMap<>();

        loginImplementation = new LoginImplementation(context);

        map.put("email",email);

        loginImplementation.forgetPassword("forget_password", map, new ApiResponse<JSONObject>() {
            @Override
            public void data(JSONObject data) {


                try {


                    loginEventMutableLiveData.setValue(LoginViewModel.LoginEvent.DATA);


                    forgetPasswordResponse.setValue(data.getString("result"));



                } catch (JSONException e) {
                    loginEventMutableLiveData.setValue(LoginEvent.ERROR);

                    e.printStackTrace();
                }


            }

            @Override
            public void Error(String message) {
                loginEventMutableLiveData.setValue(LoginViewModel.LoginEvent.ERROR);

            }
        });






    }









    private void handleFacebookAccessToken(AccessToken token, Activity activity) {
        Log.d("TAG", "handleFacebookAccessToken:" + token);

        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(activity, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d("TAG", "signInWithCredential:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            updateUI(user);
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w("TAG", "signInWithCredential:failure", task.getException());
                            updateUI(null);
                        }
                    }
                });
    }

    private void updateUI(FirebaseUser user) {

         if (user!=null) {
             socialSignIn(user.getDisplayName() == null ? user.getEmail().substring(0, 4) : user.getDisplayName()
                     , user.getEmail(), user.getProviderId(), "FB", getApplicationContext());
         }else {
             Toast.makeText(getApplicationContext(),"Your account already created using google, Please login with google",Toast.LENGTH_LONG).show();
         }


    }

    public LiveData<SignUpResponse> getResponse() {
        return signUpResponseMutableLiveData;
    }

    public LiveData<ProfileResponse> getSocialSignUpResponse() {
        return socialsignUpResponseMutableLiveData;
    }


    public LiveData<LoginEvent> getLoginEvent() {
        return loginEventMutableLiveData;
    }

    public LiveData<String> getForgetPassword() {
        return forgetPasswordResponse;
    }
}
