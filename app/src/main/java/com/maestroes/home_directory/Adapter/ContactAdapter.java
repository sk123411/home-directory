package com.maestroes.home_directory.Adapter;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;
import com.maestroes.home_directory.Activity.contactdetail.ContactDetailsActivity;
import com.maestroes.home_directory.Model.ContactModel;
import com.maestroes.home_directory.Model.ListProfileResponse;
import com.maestroes.home_directory.R;


import java.util.ArrayList;
import java.util.List;

public class ContactAdapter extends RecyclerView.Adapter<ContactAdapter.ViewHolder> {

    List<ListProfileResponse.Data> contactModelArrayList;
    Context context;

    public ContactAdapter( List<ListProfileResponse.Data> contactModelArrayList, Context context) {
        this.contactModelArrayList = contactModelArrayList;
        this.context = context;


    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.contact, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        ListProfileResponse.Data contactModel=contactModelArrayList.get(position);

        holder.txt_name.setText(contactModel.getFname());
        holder.txt_number.setText(contactModel.getMobile());
        holder.img_sea.setImageResource(R.drawable.mark);
        holder.profile_image.setImageResource(R.drawable.circleimg);
        holder.relative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context, ContactDetailsActivity.class);

                Log.d("GGGGGGGGG ","::"+contactModel.getBlockStatus());

                if (contactModel.getBlockStatus()){
                    intent.putExtra("IsContactBlock",true);
                }else {
                    intent.putExtra("IsContactBlock", false);
                }
                intent.putExtra("id",contactModel.getUserID());

                v.getContext().startActivity(intent);
            }
        });



        holder.shareButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                shareContact(contactModel.getMobile(),v.getContext());
            }
        });

        holder.callButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                calllUser(contactModel.getMobile());
            }
        });




    }

    @Override
    public int getItemCount() {
        return contactModelArrayList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        ImageView img_sea,profile_image;
        TextView txt_name,txt_number;
        RelativeLayout relative;
        ImageButton shareButton , callButton;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            img_sea = itemView.findViewById(R.id.img_sea);
            profile_image = itemView.findViewById(R.id.profile_image);
            txt_name = itemView.findViewById(R.id.txt_name);
            txt_number = itemView.findViewById(R.id.txt_number);
            relative = itemView.findViewById(R.id.relative);
            shareButton = itemView.findViewById(R.id.shareButton);
            callButton = itemView.findViewById(R.id.callButton);

        }
    }


    private void shareContact(String userNumber,Context context){

        if (userNumber!=null) {
            Intent intent = new Intent(Intent.ACTION_SEND);
            String shareMessage = "Hey i found this contact! very informative and useful, You can connect with him too " + userNumber;

            intent.setType("text/plain");
            intent.putExtra(Intent.EXTRA_SUBJECT, "Share Contact");
            intent.putExtra(Intent.EXTRA_TEXT, shareMessage);
            context.startActivity(Intent.createChooser(intent, "Share Contact"));

        }else {
            Toast.makeText(context, "User number not available! please try later", Toast.LENGTH_LONG).show();

        }
    }
    private void calllUser(String userMobileNumber) {

        if (userMobileNumber!=null) {

            Dexter.withContext(context)
                    .withPermission(Manifest.permission.CALL_PHONE)
                    .withListener(new PermissionListener() {
                        @Override
                        public void onPermissionGranted(PermissionGrantedResponse permissionGrantedResponse) {


                            String number = "tel:" + userMobileNumber;

                            if (TextUtils.isDigitsOnly(userMobileNumber)) {
                                Intent intent = new Intent(Intent.ACTION_DIAL);
                                intent.setData(Uri.parse(number));
                                context.startActivity(intent);
                            } else {
                                Toast.makeText(context, "There" +
                                        "s something wrong with user number!" + " Please try later", Toast.LENGTH_LONG).show();
                            }


                        }

                        @Override
                        public void onPermissionDenied(PermissionDeniedResponse permissionDeniedResponse) {

                        }

                        @Override
                        public void onPermissionRationaleShouldBeShown(PermissionRequest permissionRequest, PermissionToken permissionToken) {

                        }
                    }).check();
        } else {

            Toast.makeText(context, "User number not available! please try later", Toast.LENGTH_LONG).show();
        }

    }
}